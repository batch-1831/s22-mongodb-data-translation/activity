// Users Collection ===========================================
{
    "_id": "user001",
    "firstname": "John",
    "lastname": "Smith",
    "email": "johnsmith@mail.com",
    "password": "johnsmith123",
    "isAdmin": true,
    "mobileNumber": "09123456789",
    "dateTimeRegistered": "2022-06-10T15:00:00.00Z"
}

{
    "_id": "user002",
    "firstname": "Jane",
    "lastname": "Doe",
    "email": "janedoe@mail.com",
    "password": "janedoe123",
    "isAdmin": false,
    "mobileNumber": "09123123123",
    "dateTimeRegistered": "2022-06-10T15:00:00.00Z"
}


// Products Collection ===========================================
// Pencil
{
    "_id": "product001",
    "name": "pencil",
    "description": "A tool to be used to write on paper",
    "price": 10,
    "stocks": 92, //original stocks: 100
    "isActive": true,
    "sku": "MDR-10203"
}

// Sharpener
{
    "_id": "product002",
    "name": "sharpener",
    "description": "A tool to be used to sharpen a pencil",
    "price": 40,
    "stocks": 0,
    "isActive": false,
    "sku": "MDR-10225"
}

// Eraser
{
    "_id": "product003",
    "name": "eraser",
    "description": "A tool to be used to erase markings or smudges made by the pencil",
    "price": 20,
    "stocks": 45, //original stocks: 60
    "isActive": true,
    "sku": "MDR-10322"
}


// Order Products Collection ===========================================
// Batch 1 Orders
{
    "_id": "orderProducts001",
    "orderID": "order001",
    "productID": "product001",
    "quantity": 5, //5 pencils
    "price": 10, //price of one pencil
    "subtotal": [50]
}

{
    "_id": "orderProducts002",
    "orderID": "order001",
    "productID": "product003",
    "quantity": 10, //10 erasers
    "price": 20, //price of one eraser
    "subtotal": [200]
}

// Batch 2 Orders
{
    "_id": "orderProducts003",
    "orderID": "order002",
    "productID": "product001",
    "quantity": 3, //3 pencils
    "price": 10, //price of one pencil
    "subtotal": [30]
}

{
    "_id": "orderProducts004",
    "orderID": "order002",
    "productID": "product003",
    "quantity": 5, //5 erasers
    "price": 20, //price of one eraser
    "subtotal": [100]
}


// Orders Collection ===========================================
{
    "_id": "order001",
    "userID": "user002",
    "transactionDate": "2022-06-10T15:00:00.00Z",
    "status": "The products were paid and is being shipped",
    "total": 250
}

{
    "_id": "order002",
    "userID": "user001",
    "transactionDate": "2022-06-10T15:00:00.00Z",
    "status": "The products were paid and is being shipped",
    "total": 130
}
